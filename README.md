**Zkušební projekt pro nového FE vývojáře**

**Instalace:**

1.Stáhněte a nainstalujte docker + docker-compose
 - Windows: https://store.docker.com/editions/community/docker-ce-desktop-windows
 - MacOs: https://store.docker.com/editions/community/docker-ce-desktop-mac
 
2.Stáhněte a nainstalujte si NodeJS LTS.
 - https://nodejs.org/en/
 
**Spuštění:**

1.Nainstalujte si NPM balíčky - `npm install`. (nejprve nutno se přepnout do složky projektu/shop)

2.Vytvořte docker image
 - v rootu projektu spustit: `docker build --no-cache -t vivantis/new-frontend-developer .`

3.Vytvořte a spusťte kontejner s projektem
 - v rootu projektu `docker-compose up `

4.Projekt by měl být dostupný na `127.0.0.1`

Kompilace SASS a JS (spouštějte ve složce shop):
1. Jednorázová kompilace: `npm run build`
2. Automatická kompilace (watchování změn): `npm run build-watch`
3. Produkční kompilace: `npm run build-prod`

**Zadání:**

Nastylujte s pomocí frameworku bootstrap 4, HP e-shopu https://www.vivantis.cz. Hlavička, stačí bez submenu, vrchní část HP - slider, malé bannery, USP (vpravo od malých banerů) a tři bloky s inspirací (pod sliderem a banery). Konvence pojmenování bude tvořena za pomocí BEM (http://getbem.com/).

- Stránku přizpůsobte mobilním zařízením - využíjte k tomu BS breakpointy

**Struktura složek:** 
 - SASS (css) - shop/css (dílčí soubory linkujte do spolčeného main.scss)
 - Js - shop/js - potřebné js přidejte do souboru layout.js
 - Templates - shop/templates. Syntaxe nette/latte https://latte.nette.org/cs/

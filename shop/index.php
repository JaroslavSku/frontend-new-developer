<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require __DIR__ . '/vendor/latte/init.php';
require __DIR__ . '/vendor/tracy/tracy/src/tracy.php';

use Tracy\Debugger;

Debugger::enable(Debugger::DEVELOPMENT, __DIR__ . "/logs");

renderTemplate("index", "homepage", array(
	'_PAGE' => ["title" => "Vivantis - Testovací projekt frontend"]
));